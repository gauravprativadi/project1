#include "ofApp.h"
#include <ctime>

//--------------------------------------------------------------
void ofApp::setup(){
	// Setup grabber
	grabber.setup(1280, 700);
	fbo.allocate(1280, 700);
	
	ofDisableArbTex();
	texture = make_shared<ofTexture>();
	ofLoadImage(*texture, "textures/tex.png");
	texture->setTextureMinMagFilter(GL_NEAREST, GL_NEAREST);
	texture->setTextureWrap(GL_REPEAT, GL_REPEAT);
	ofEnableArbTex();
	
	// All examples share data files from example-data, so setting data path to this folder
	// This is only relevant for the example apps
	ofSetDataPathRoot(ofFile("model/"));

	// Setup tracker
	tracker.setup();

	box2d.init();
	box2d.setGravity(0, 10);
	box2d.setFPS(60.0);
	box2d.registerGrabbing();

	collisionFluid.allocate(1280, 700, 0.5); //setup the fluid sim and use half resolution to improve fps

	// Seting the gravity and dissipation rates
	collisionFluid.dissipation = 0.99;
	collisionFluid.velocityDissipation = 0.99;
	collisionFluid.setGravity(ofVec2f(0.0, 0.0));

	trailFluid.allocate(1280, 700, 0.5);
	trailFluid.dissipation = 0.9;
	trailFluid.velocityDissipation = 0.99;
	trailFluid.setGravity(ofVec2f(0, 0));

	m_fluid.allocate(1289, 700, 0.5);
	m_fluid.dissipation = 0.6;
	m_fluid.velocityDissipation = 0.55;
	m_fluid.setGravity(ofVec2f(0, 0));


	startTime = clock(); //Start timer
	secondsToDelay = 5;
	secondsPassed = 0;
	is_collision = false;
}

//--------------------------------------------------------------
void ofApp::update(){
	grabber.update(); //update the camera

	box2d.update();
   // Update tracker when there are new frames
	if (grabber.isFrameNew()) {
		fbo.begin();
		grabber.draw(grabber.getWidth(), 0, -grabber.getWidth(), grabber.getHeight());
		fbo.end();

		fbo.readToPixels(pix);
		pix.setImageType(ofImageType::OF_IMAGE_GRAYSCALE);

		tracker.update(ofxCv::toCv(pix));
	}
	
	//Spawn Apples
	secondsPassed = (clock() - startTime) / CLOCKS_PER_SEC;
	if (secondsPassed >= secondsToDelay)
	{
		spawnSphere();
		secondsPassed = 0;
		startTime = clock();
	}


	// Iterate over all faces
	for (auto face : tracker.getInstances()) { //go thru all faces found
		
		ofxFaceTracker2Landmarks markers = face.getLandmarks(); //get the landmarks for this face
		glm::vec2 tLip = markers.getImagePoint(51);
		glm::vec2 bLip = markers.getImagePoint(57);
		l_Dist = ofDist(tLip.x, tLip.y, bLip.x, bLip.y);

		// Draw a polyline with the mouth as an outline
		for (int i = 48; i < 60; i++)
		{
			outline.addVertex(markers.getImagePoint(i).x, markers.getImagePoint(i).y, 0);
		}
		outline.close();
	
		float time = ofGetElapsedTimef();

		//Color and fluid for the splash on collision
		ofFloatColor splashColor(ofNoise(time / 1.1)*5.0f - 0.5f, ofNoise(time / 0.3f)*0.5f - 0.5f, 0.3f);
		
		if (is_collision) {
			collisionFluid.addTemporalForce(collisionEmitter, ofPoint(sin(time / 3.0f), l_Dist), splashColor, 5.0f);
			is_collision = false;
		}

		//Calculation for emitters for the eyes
		float distx = ofDist(markers.getImagePoint(39).x, markers.getImagePoint(39).y, markers.getImagePoint(36).x, markers.getImagePoint(36).y);
		float disty = ofDist(markers.getImagePoint(40).x, markers.getImagePoint(40).y, markers.getImagePoint(38).x, markers.getImagePoint(38).y);
		glm::vec2 midP = glm::vec2(markers.getImagePoint(36).x + distx / 2, markers.getImagePoint(38).y + disty/2);

		l_fluidEmitter += (midP - l_fluidEmitter) * 0.3;

		float dist1x = ofDist(markers.getImagePoint(45).x, markers.getImagePoint(45).y, markers.getImagePoint(42).x, markers.getImagePoint(42).y);
		float dist1y = ofDist(markers.getImagePoint(47).x, markers.getImagePoint(47).y, markers.getImagePoint(43).x, markers.getImagePoint(43).y);
		glm::vec2 midP1 = glm::vec2(markers.getImagePoint(42).x + dist1x / 2, markers.getImagePoint(42).y + dist1y / 2);

		r_fluidEmitter += (midP1 - r_fluidEmitter) * 0.3;

		//Color and Fluid for the eyes
		ofFloatColor fluidColor(ofNoise(time / 2.0)*2.0 - 0.5, ofNoise(time / 0.7)*1.0 - 0.5, 0.5);
		if (l_Dist > 37) {
			m_fluid.addTemporalForce(l_fluidEmitter, ofPoint(sin(time / 4.0), -l_Dist), fluidColor, l_Dist / 4);
			m_fluid.addTemporalForce(r_fluidEmitter, ofPoint(sin(time / 4.0), -l_Dist), fluidColor, l_Dist / 4);
		}

	}

	//Check for collision with apple and mouth
	for (int i = 0 ; i < spheres.size() ; i++) 
	{	
		if (outline.inside(spheres[i]->getPosition().x, spheres[i]->getPosition().y) && l_Dist > 38)
		{
			collisionEmitter = spheres[i]->getPosition();
			spheres.erase(spheres.begin() + i);
			texture_list.erase(texture_list.begin() + i);
			is_collision = true;
		}
		
		if (spheres[i]->getPosition().y > ofGetHeight())
		{
			spheres.erase(spheres.begin() + i);
			texture_list.erase(texture_list.begin() + i);
		}

		//Color and fluid for the trail 
		float time = ofGetElapsedTimef();
		ofFloatColor trailColor(ofNoise(time / 1.1)*5.0 - 0.5, ofNoise(time / 1.1)*3.0 - 0.5, 1.0);
		glm::vec2 dir = spheres[i]->getVelocity().normalize();
		trailFluid.addTemporalForce(spheres[i]->getPosition(), dir, trailColor, 7.0);
	}

	collisionFluid.update();
	trailFluid.update();
	m_fluid.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
	
	//grabber.draw(0, 0);

	ofEnableAlphaBlending();
	fbo.draw(0, 0);
	collisionFluid.draw(); //draw the fluid
	trailFluid.draw();
	m_fluid.draw();

	//Drawing the apples
	for (int i = 0; i < spheres.size(); i++)
	{
		ofPushStyle();
		ofFill();
		ofSetColor(255);
		//spheres[i]->draw();
		texture_list[i]->setAnchorPercent(0.5, 0.5);
		texture_list[i]->draw(spheres[i]->getPosition());
		ofPopStyle();
	}
	ofDisableAlphaBlending();

	/*
	for (auto face : tracker.getInstances()) {
		ofPushStyle();
		ofSetColor(0);
		ofNoFill();
		outline.draw();
		ofPopStyle();
		outline.clear();	
	}*/

}

void ofApp::spawnSphere() {

	int val = (int)ofRandom(0,2);
	float xPos;
	int mult;
	switch (val) {
	case 0:
		xPos = 10.0f;
		mult = 1;
		break;

	case 1: 
		xPos = ofGetWidth() - 10;
		mult = -1;
		break;
	default:
		xPos = 25;
		break;

	}
	float yPos = ofRandom(100, ofGetHeight()/2);

	ofxBox2dCircle* circle = new ofxBox2dCircle();
	circle->setPhysics(0.3, 0.53, 0);
	circle->setup(box2d.getWorld(), xPos, yPos, 2);
	spheres.push_back(circle);
	texture_list.push_back(texture);
	//sphereColor.push_back(ofColor(ofRandom(0, 255), ofRandom(0, 255), ofRandom(0, 255)));

	ofVec3f frc;
	frc = glm::vec3(2.5 * mult, -1, 0);
	frc.normalize();
	circle->addForce(frc, ofRandom(2, 3));
}


#pragma once

#include "ofMain.h"
#include "ofxFaceTracker2.h"
#include "ofxBox2d.h"
#include "ofxFluid.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		void spawnSphere();

		ofPolyline								outline;
		ofVideoGrabber							grabber;
		ofFbo									fbo;
		ofxFaceTracker2							tracker;
		ofxBox2d                                box2d;
		vector<ofxBox2dCircle*>					spheres;
		double									secondsPassed;
		double									secondsToDelay;
		bool									is_collision;
		ofPixels								pix;
		clock_t									startTime;
		shared_ptr<ofTexture>					texture;
		vector<shared_ptr<ofTexture>>			texture_list;
		ofRectangle								rect;
		ofxFluid								collisionFluid;
		ofxFluid								trailFluid;
		ofxFluid								m_fluid;
		glm::vec2								collisionEmitter;
		glm::vec2								collisionPoint;
		glm::vec2								l_fluidEmitter;
		glm::vec2								r_fluidEmitter;
		float									l_Dist;
};
